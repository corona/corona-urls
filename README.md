# covid-urls

If you're searching for machine-readable content look to [/git_urls](git_urls) or [/http_urls](http_urls).
The following URLs are human-readable content.
If you want Open-Source-Projects handling Corona data look at [/PROJECTS.md](PROJECTS.md)

### Global
- [CSSE: https://www.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6](https://www.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6)
- [https://covid19.who.int](https://covid19.who.int/)

### Germany

- [https://corona.rki.de](https://experience.arcgis.com/experience/478220a4c454480e823b17327b2bf1d4)
- [https://risikotabelle.de](https://pavelmayer.de/covid/risks/)
- [https://impfdashboard.de](https://impfdashboard.de/)
- [https://intensivregister.de](https://www.intensivregister.de/#/index)

##### NRW

- [https://www.mags.nrw/coronavirus-fallzahlen-nrw](https://www.mags.nrw/coronavirus-fallzahlen-nrw)
- [https://www.lzg.nrw.de/covid19/daten/laborbest_faelle_sars-cov-2.pdf](https://www.lzg.nrw.de/covid19/daten/laborbest_faelle_sars-cov-2.pdf)

##### Hessen
- [https://soziales.hessen.de/gesundheit/corona-in-hessen/taegliche-uebersicht-der-bestaetigten-sars-cov-2-faelle](https://soziales.hessen.de/gesundheit/corona-in-hessen/taegliche-uebersicht-der-bestaetigten-sars-cov-2-faelle)

##### Cologne

- [https://stadt-koeln.de](https://www.stadt-koeln.de/)
- [https://stadt-koeln.de/leben-in-koeln/gesundheit/infektionsschutz/corona-virus/corona-virus-koeln-entwicklung-der-fallzahlen](https://www.stadt-koeln.de/leben-in-koeln/gesundheit/infektionsschutz/corona-virus/corona-virus-koeln-entwicklung-der-fallzahlen)
