These are Projects that uses Corona APIs or parse a API and delivier it in any form.

### Global

- https://github.com/ard-data/covid-19_john_hopkins

### Germany

- https://github.com/ard-data/2020-rki-impf-archive
- https://github.com/ard-data/2020-rki-archive
- https://github.com/favstats/vaccc19de_dashboard
- https://github.com/n0rdlicht/rki-vaccination-scraper
- https://github.com/mathiasbynens/covid-19-vaccinations-germany
- https://github.com/untergeekDE/scrape-covid19

##### NRW

- https://github.com/wdr-data/covid19_nrw
- https://github.com/wdr-data/python_covid19_nrw
